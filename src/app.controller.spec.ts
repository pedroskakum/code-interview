import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { ReaderService } from './reader/reader.service';
import { CoordinatesService } from './coordinates/coordinates.service';
import { TimerService } from './timer/timer.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [ReaderService, CoordinatesService, TimerService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('getForm', () => {
    it('should return html string', async () => {
      const getResult = await appController.getForm();
      expect(getResult).toBeDefined();
      expect(getResult).toContain('form');
    });
  });

  describe('getResult', () => {
    it('should return array', async () => {
      const getResult = await appController.getResult('TESTE001');
      expect(getResult).toEqual(expect.any(Array));
    });

    it('should return error when plate is not found in data', async () => {
      const getResult = await appController.getResult('PLACA002');
      expect(getResult).toEqual('Error: placa não encontrada!');
    });

    it('should return error when plate is not passed', async () => {
      const getResult = await appController.getResult('');
      expect(getResult).toEqual('Error: placa está vazia!');
    });
  });
});
