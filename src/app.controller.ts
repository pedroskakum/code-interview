
import { Controller, Get, Post, Body } from '@nestjs/common';
import { ReaderService } from './reader/reader.service';
import { CoordinatesService } from './coordinates/coordinates.service';
import { TimerService } from './timer/timer.service';
import { Position } from './interfaces/position.interface';
import { Poi } from './interfaces/poi.interface';
import { Result } from './interfaces/result.interface';

@Controller()
export class AppController {
  pois: Array<Poi>;
  positions: Array<Position>;
  results: Array<Result> = [];
  previousPoiElement: Poi;

  constructor(
    private readonly readerService: ReaderService,
    private readonly coordinatesService: CoordinatesService,
    private readonly timerService: TimerService,
  ) { }

  @Get()
  getForm(): string {
    return '<form method="post">Placa:<input name="placa" type="text"><input type="submit"></form>';
  }

  @Post()
  async getResult(@Body('placa') placa: string): Promise<any> {
    try {
      if (!placa) {
        throw new Error('placa está vazia!');
      }

      this.positions = await this.readerService.getCsvData('posicoes', placa);
      this.pois = await this.readerService.getCsvData('pois');
      const data = [];

      if (!this.positions.length) {
        throw new Error('placa não encontrada!');
      }
      this.previousPoiElement = new Poi();
      this.positions.forEach((positionElement) => {
        this.pois.forEach((poiElement) => {
          const distance = this.coordinatesService.shortDistanceBetweenTwoPointsInSphere(
            {
              lat1: positionElement.latitude,
              lon1: positionElement.longitude,
            },
            {
              lat2: poiElement.latitude,
              lon2: poiElement.longitude,
            },
          );

          if (distance < poiElement.raio) {
            if (this.previousPoiElement && this.previousPoiElement.nome === poiElement.nome) {
              data[data.length - 1].date.end = new Date(positionElement.data_posicao);
            } else {
              data.push({
                poi: poiElement.nome,
                date: {
                  start: new Date(positionElement.data_posicao)
                }
              })
            }
            this.previousPoiElement = poiElement;
          }
        });
      });

      this.results = data
        .filter(result => result.date.end !== undefined)
        .map(result => {
          return {
            poi: result.poi,
            ...result.date,
            milliseconds: result.date.end - result.date.start,
            time: this.timerService.formatDifferenceBetweenDates(result.date.end - result.date.start)
          }
        });

      return this.results;
    } catch (e) {
      console.error(e);
      return `${e}`;
    }
  }
}
