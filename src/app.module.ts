import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ReaderService } from './reader/reader.service';
import { CoordinatesService } from './coordinates/coordinates.service';
import { TimerService } from './timer/timer.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [ReaderService, CoordinatesService, TimerService],
})
export class AppModule {}
