import { Test, TestingModule } from '@nestjs/testing';
import { CoordinatesService } from './coordinates.service';

describe('CoordinatesService', () => {
  let service: CoordinatesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CoordinatesService],
    }).compile();

    service = module.get<CoordinatesService>(CoordinatesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('shortDistanceBetweenTwoPointsInSphere', () => {
    it('should return correct distance(m) in south of the equator and west of meridian', async () => {
      const coordenate1 = {
        lat1: -25.3649568,
        lon1: -51.4699475,
      };
      const coordenate2 = {
        lat2: -25.56742701740896,
        lon2: -51.47653363645077,
      }

      const distance = await service.shortDistanceBetweenTwoPointsInSphere(coordenate1, coordenate2);
      expect(distance).toBe(22523.36789749307);
    });

    it('should return correct distance(m) in north of the equator and east of meridian', async () => {
      const coordenate1 = {
        lat1: 48.97097,
        lon1: 122.02893,
      };
      const coordenate2 = {
        lat2: 48.965496,
        lon2: 122.072989,
      }

      const distance = await service.shortDistanceBetweenTwoPointsInSphere(coordenate1, coordenate2);
      expect(distance).toBe(3273.26433185412);
    });
  });
});
