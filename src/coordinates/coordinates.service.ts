import { Injectable } from '@nestjs/common';

@Injectable()
export class CoordinatesService {
  /**
   *    Haversine Formula
   *
   *  a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
   *  c = 2 ⋅ atan2( √a, √(1−a) )
   *  d = R ⋅ c
   */
  shortDistanceBetweenTwoPointsInSphere(
    { lat1, lon1 },
    { lat2, lon2 },
  ): number {
    const R = 6371e3; // r of earth in metres
    const φ1 = (lat1 * Math.PI) / 180; // φ, λ in radians
    const φ2 = (lat2 * Math.PI) / 180;
    const Δφ = ((lat2 - lat1) * Math.PI) / 180;
    const Δλ = ((lon2 - lon1) * Math.PI) / 180;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c; // in metres

    return d;
  }
}
