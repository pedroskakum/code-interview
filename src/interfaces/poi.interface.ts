export class Poi {
  nome: string;
  raio: number;
  latitude: number;
  longitude: number;
}
