export class Position {
  placa: string;
  data_posicao: Date;
  velocidade: number;
  longitude: number;
  latitude: number;
  ignicao: boolean;
}
