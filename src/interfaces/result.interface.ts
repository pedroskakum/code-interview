export class Result {
    poi: string;
    date: {
        start: Date,
        end?: Date,
        milliseconds?: number
    };
    time?: Date
}
