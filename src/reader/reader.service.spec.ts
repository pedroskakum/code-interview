import { Test, TestingModule } from '@nestjs/testing';
import { ReaderService } from './reader.service';

describe('ReaderService', () => {
  let service: ReaderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReaderService],
    }).compile();

    service = module.get<ReaderService>(ReaderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getFileData', () => {
    it('should be defined', () => {
      expect(service.getCsvData).toBeDefined();
    });

    it('should return correct data with no filter', async () => {
      const data = await service.getCsvData('posicoes');
      expect(data.length).toBe(928);
      expect(data).toBeInstanceOf(Array);
    });

    it('should return correct data passing TESTE001 filter', async () => {
      const data = await service.getCsvData('posicoes', 'TESTE001');
      expect(data.length).toBe(722);
      expect(data).toBeInstanceOf(Array);
    });

    it('should return correct data passing CAR0012 filter', async () => {
      const data = await service.getCsvData('posicoes', 'CAR0012');
      expect(data.length).toBe(206);
      expect(data).toBeInstanceOf(Array);
    });

  });
});
