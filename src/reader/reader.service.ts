import { Injectable } from '@nestjs/common';
import { createReadStream } from 'fs';
import * as csv from 'csv-parser';

@Injectable()
export class ReaderService {
  async getCsvData(fileName: string, plateFilter?: string): Promise<any> {
    const results = [];
    const stream = createReadStream(`data/${fileName}.csv`).pipe(csv());
    for await (const chunk of stream) {
      if (plateFilter && chunk.placa) {
        if (chunk.placa === plateFilter) {
          results.push(chunk);
        }
      } else {
        results.push(chunk);
      }
    }
    return results;
  }
}
