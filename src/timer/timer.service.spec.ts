import { Test, TestingModule } from '@nestjs/testing';
import { TimerService } from './timer.service';

describe('TimerService', () => {
  let service: TimerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TimerService],
    }).compile();

    service = module.get<TimerService>(TimerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('formatDifferenceBetweenDates', () => {
    it('should return correct conversion of milliseconds in the best unit with approximate value', async () => {
      const distance = await service.formatDifferenceBetweenDates(12546233);
      expect(distance).toBe("3 hour(s)");
    });

    it('should return correct conversion of 3 hours', async () => {
      const distance = await service.formatDifferenceBetweenDates(10800000);
      expect(distance).toBe("3 hour(s)");
    });

    it('should return correct conversion of 2 hours', async () => {
      const distance = await service.formatDifferenceBetweenDates(10799999);
      expect(distance).toBe("2 hour(s)");
    });
  });
});
