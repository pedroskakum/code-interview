import { Injectable } from '@nestjs/common';

@Injectable()
export class TimerService {

    formatDifferenceBetweenDates(milliseconds: number): string {
        let difference = '';
        const diffTime = milliseconds;

        const diffDays = Math.floor(diffTime / (1000 * 60 * 60 * 24));
        if (diffDays >= 1 && diffDays) {
            difference = `${diffDays} day(s)`;
        }

        const diffHours = Math.floor(diffTime / (1000 * 60 * 60));
        if (diffHours >= 1 && diffHours < 24) {
            difference = `${diffHours} hour(s)`;
        }

        const diffMinutes = Math.floor(diffTime / (1000 * 60));
        if (diffMinutes >= 1 && diffMinutes < 60) {
            difference = `${diffMinutes} minute(s)`;
        }

        const diffSeconds = Math.floor(diffTime / 1000);
        if (diffSeconds >= 1 && diffSeconds < 60) {
            difference = `${diffSeconds} second(s)`;
        }

        if (diffSeconds < 1) {
            difference = `${diffTime} milliseconds`;
        }

        return difference;
    }
}
